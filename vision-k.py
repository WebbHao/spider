#coding=utf-8

#urllib模块提供了读取Web页面数据的接口
import urllib
import os
#re模块主要包含了正则表达式
import re
import datetime


#统计获取图片总数
_imgLen = 0
#定义一个getHtml()函数
def getHtml(url):
    print(url)
    page = urllib.urlopen(url)  #urllib.urlopen()方法用于打开一个URL地址
    html = page.read() #read()方法用于读取URL上的数据
    return html

def getImg(html,path):
    global _imgLen
    reg = r'href=(.+?\.jpg)'    #正则表达式，得到图片地址
    #reg = r'(jpg$) pic_ext'
    imgre = re.compile(reg)     #re.compile() 可以把正则表达式编译成一个正则表达式对象.
    imglist = imgre.findall(html)      #re.findall() 方法读取html 中包含 imgre（正则表达式）的    数据
    #print imglist
    #把筛选的图片地址通过for循环遍历并保存到本地
    #核心是urllib.urlretrieve()方法,直接将远程数据下载到本地，图片通过x依次递增命名
    x = 1
    path = path+'\%s.jpg'
    for imgurl in imglist:
        urllib.urlretrieve(imgurl,path % x)
        x+=1
        _imgLen+=1
        print(str(x)+':'+imgurl+' 在'+path+'保存成功')

# 创建新目录
def mkdir(path):
    path = path.strip()
    # 判断路径是否存在
    # 存在     True
    # 不存在   False
    isExists = os.path.exists(path)
    # 判断结果
    if not isExists:
        # 如果不存在则创建目录
        print u"偷偷新建了名字叫做", path, u'的文件夹'
        # 创建目录操作函数
        os.makedirs(path)
        return True
    else:
        # 如果目录存在则不创建，并提示目录已存在
        print u"名为", path, '的文件夹已经创建成功'
        return False

def getTargetURL(url):
    page    = urllib.urlopen(url)  # urllib.urlopen()方法用于打开一个URL地址
    html    = page.read()          # read()方法用于读取URL上的数据
    #<li><a href=photo.asp?id=19>Life</a><br /></li>
    reg     = '<li><a href=(.*?)>(.*?)</a><br />.*?</li>'
    regURL  = re.compile(reg)
    urlList = regURL.findall(html)
    return  urlList
#http://www.vision-k.net/Archiver/photo.asp?id=19
#http://www.vision-k.net/Archiver/photo.asp?id=18
#http://www.vision-k.net/Archiver/photo.asp?id=17
#http://www.vision-k.net/Archiver/photo.asp?id=16
#http://www.vision-k.net/Archiver/photo.asp?id=14
#http://www.vision-k.net/Archiver/slide.asp?id=1
#html = getHtml("http://www.vision-k.net/Archiver/slide.asp?id=1")
#print(getImg(html))


'''
定义main 函数
'''
def main():
    target     = 'http://www.vision-k.net/Archiver/photoclass.asp?id=11'
    basePath   = 'E:\Personal\IMGAGE\\vision-k'
    baseURL    = 'http://www.vision-k.net/Archiver/'
    targetURLs = getTargetURL(target)
    dirLen     = len(targetURLs)
    #print(targetURLs)

    for value in targetURLs:
        path = basePath+'\\'+value[1]
        mkdir(path)
        html = getHtml(baseURL+value[0])
        getImg(html,path)
    print('总计操作目录：'+str(dirLen))
    print('总计抓取图片：'+str(_imgLen)+'张')


if __name__ == '__main__':
    print '抓取VISION-K图片开始'
    startTime = datetime.datetime.now()
    main()
    endTime = datetime.datetime.now()
    print('抓取VISION-K图片结束')
    print('总计运行时长：'+str((endTime-startTime).seconds))
